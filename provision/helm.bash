#!/bin/bash

function upgrade() {

    echo "Upgrading ${SERVICE_NAME}"

    helm upgrade -f ./chart/${SERVICE_NAME}/values.${ENVIRONMENT}.yaml ${SERVICE_NAME} \
     -i --force --namespace default --timeout 480 \
     --set deployment.containers.service.image.tag=${DOCKER_TAG} \
     --set deployment.containers.service.image.repository=${DOCKER_REPOSITORY}  \
     --wait --debug ./chart/${SERVICE_NAME}

}

function rollback() {

    echo "Error deploying to ${CLOUDSDK_CONTAINER_CLUSTER}, either the deployment timed out after 480 seconds or you deployment is not healthy. Rolling back to previous version."
    ROLLBACK_VERSION=$(helm history ${SERVICE_NAME} | grep -v \"Rollback\" | grep DEPLOYED | awk '{print $1}')
    helm rollback ${SERVICE_NAME} ${ROLLBACK_VERSION}
    exit 1

}

function action() {
    helm lint ./chart/${SERVICE_NAME}/

    echo "Upgrading ${SERVICE_NAME} on cluster ${CLOUDSDK_CONTAINER_CLUSTER}-${CLOUDSDK_COMPUTE_ZONE}"
    upgrade || rollback
}

var_usage() {
        cat <<EOF
No cluster is set. To set the cluster (and the region/zone where it is found), set the environment variables
  CLOUDSDK_COMPUTE_REGION=<cluster region> (regional clusters)
  CLOUDSDK_COMPUTE_ZONE=<cluster zone> (zonal clusters)
  CLOUDSDK_CONTAINER_CLUSTER=<cluster name>
EOF
        exit 1
}

#init

function docker_image_tag() {
    if [[ -z "${TAG_NAME}" ]]; then
      DOCKER_REPOSITORY=gcr.io/${PROJECT_ID}/service/${SERVICE_NAME}
      DOCKER_TAG=${COMMIT_SHA}
    else
      DOCKER_REPOSITORY=gcr.io/${PROJECT_ID}/service/${SERVICE_NAME}
      DOCKER_TAG=${TAG_NAME}
    fi
}

function config_cluster_context() {

    # If there is no current context, get one.
    if [[ $(kubectl config current-context 2> /dev/null) == "" ]]; then
        # This tries to read environment variables. If not set, it grabs from gcloud
        cluster=${CLOUDSDK_CONTAINER_CLUSTER:-$(gcloud config get-value container/cluster 2> /dev/null)}
        region=${CLOUDSDK_COMPUTE_REGION:-$(gcloud config get-value compute/region 2> /dev/null)}
        zone=${CLOUDSDK_COMPUTE_ZONE:-$(gcloud config get-value compute/zone 2> /dev/null)}
        project=${GCLOUD_PROJECT:-$(gcloud config get-value core/project 2> /dev/null)}


        [[ -z "$cluster" ]] && var_usage
        [ ! "$zone" -o "$region" ] && var_usage

        if [ -n "$region" ]; then
          echo "Running: gcloud config set container/use_v1_api_client false"
          gcloud config set container/use_v1_api_client false
          echo "Running: gcloud beta container clusters get-credentials --project=\"$project\" --region=\"$region\" \"$cluster\""
          gcloud beta container clusters get-credentials --project="$project" --region="$region" "$cluster" || exit
        else
          echo "Running: gcloud container clusters get-credentials --project=\"$project\" --zone=\"$zone\" \"$cluster\""
          gcloud container clusters get-credentials --project="$project" --zone="$zone" "$cluster" || exit
        fi
    fi
}

function prepare_helm() {

    echo "Running: helm init --client-only"
    helm init --client-only

    # if GCS_PLUGIN_VERSION is set, install the plugin
    if [[ -n $GCS_PLUGIN_VERSION ]]; then
      echo "Installing helm GCS plugin version $GCS_PLUGIN_VERSION "
      helm plugin install https://github.com/nouney/helm-gcs --version $GCS_PLUGIN_VERSION
    fi

    # check if repo values provided then add that repo
    if [[ -n $HELM_REPO_NAME && -n $HELM_REPO_URL ]]; then
      echo "Adding chart helm repo $HELM_REPO_URL "
      helm repo add $HELM_REPO_NAME $HELM_REPO_URL
    fi

    echo "Running: helm repo update"
    helm repo update
}

# act

docker_image_tag && config_cluster_context && prepare_helm


# if 'TILLERLESS=true' is set, run a local tiller server with the secret backend
# see also https://github.com/helm/helm/blob/master/docs/securing_installation.md#running-tiller-locally
if [ "$TILLERLESS" = true ]; then
  # create tiller-namespace if it doesn't exist (helm --init would usually do this with server-side tiller'
  if [[ -n $TILLER_NAMESPACE ]]; then
    echo "Creating tiller namespace $TILLER_NAMESPACE"
    kubectl get namespace $TILLER_NAMESPACE || kubectl create namespace $TILLER_NAMESPACE
  fi

  echo "Starting local tiller server"
  #default inherits --listen localhost:44134 and TILLER_NAMESPACE
  #use the secret driver by default
  tiller --storage=secret &
  export HELM_HOST=localhost:44134

  action #

  exitCode=$?
  echo "Stopping local tiller server"
  pkill tiller
  exit $exitCode
else
  action
fi