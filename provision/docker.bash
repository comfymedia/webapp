#!/bin/bash

if [[ -z "${TAG_NAME}" ]]; then
  DOCKER_REPOSITORY=gcr.io/${PROJECT_ID}/service/${SERVICE_NAME}
  DOCKER_TAG=${COMMIT_SHA}
else
  DOCKER_REPOSITORY=gcr.io/${PROJECT_ID}/service/${SERVICE_NAME}
  DOCKER_TAG=${TAG_NAME}
fi

echo "Dockerizing"
echo "Building ${DOCKER_REPOSITORY}:${DOCKER_TAG}"

docker build -t "${DOCKER_REPOSITORY}:${DOCKER_TAG}" .

docker push "${DOCKER_REPOSITORY}:${DOCKER_TAG}"