import tinycolor from "tinycolor2";

const primary = "#efc02f";
const secondary = "#897b8f";
const warning = "#ffc31a";
const success = "#8bd466";
const info = "#3481ff";

const extraLightenRate = 40;
const mediumLightenRate = 15;
const lightenRate = 7.5;
const darkenRate = 15;

export default {
  drawer: {
    drawerWidth: 300
  },
  zIndex: {
    drawer: 100
  },
  images: {
  },
  gridItem: {
    breakpoints: {
      xs: {
        width: 395,
      },
      sm: {
        width: 395,
      },
      md: {
        width: 540,
      },
      lg: {
        width: 540,
      },
      xl: {
        width: 540,
      }
    }
  },
  transitions: {
    duration: {
      enteringScreen: 250,
      leavingScreen: 250
    }
  },
  palette: {
    primary: {
      main: primary,
      extraLight: tinycolor(primary)
        .lighten(extraLightenRate)
        .toHexString(),
      mediumLight: tinycolor(primary)
        .lighten(mediumLightenRate)
        .toHexString(),
      light: tinycolor(primary)
        .lighten(lightenRate)
        .toHexString(),
      dark: tinycolor(primary)
        .darken(darkenRate)
        .toHexString()
    },
    secondary: {
      main: secondary,
      light: tinycolor(secondary)
        .lighten(lightenRate)
        .toHexString(),
      dark: tinycolor(secondary)
        .darken(darkenRate)
        .toHexString(),
      contrastText: "#FFFFFF"
    },
    warning: {
      main: warning,
      light: tinycolor(warning)
        .lighten(lightenRate)
        .toHexString(),
      dark: tinycolor(warning)
        .darken(darkenRate)
        .toHexString()
    },
    success: {
      main: success,
      light: tinycolor(success)
        .lighten(lightenRate)
        .toHexString(),
      dark: tinycolor(success)
        .darken(darkenRate)
        .toHexString()
    },
    info: {
      main: info,
      light: tinycolor(info)
        .lighten(lightenRate)
        .toHexString(),
      dark: tinycolor(info)
        .darken(darkenRate)
        .toHexString()
    },
    text: {
      primary: "#000000",
      secondary: "rgba(0,0,0,0.70)",
      hint: "#B9B9B9"
    },
    background: {
      disabled: "#858585",
      default: "#ffffff",
      light: "#F3F5FF",
      shaded: "rgb(249,249,249)",
      watermark: "rgb(249,249,249, 0.7)",
      overlay: "rgb(0, 0, 0, 0.6)"
    },
    charts: {
      colors: [
        "#ED173D",
        "#87B538",
        "#191970",
        "#D28F4A",
        "#90D9AA",
        "#9CAA4B",
        "#D5C770",
        "#767842",
        "#224A4F",
        "#0C2F2E",
        "#10905F",
        "#A8E470",
        "#FBBF37",
        "#F25028",
        "#BA2136",
        "#EDD3B0",
        "#526783",
        "#DA954A",
        "#7F9348",
        "#9400D3",
        "#C0C0C0",
        "#AFEEEE",
        "#778899",
        "#F0FFFF",
        "#40E0D0",
        "#F5FFFA",
        "#9932CC",
        "#FF8C00",
        "#FF6347",
        "#696969",
        "#32CD32",
        "#D2691E",
        "#FFFFE0",
        "#556B2F",
        "#FFFAF0",
        "#E6E6FA",
        "#FA8072",
        "#F0E68C",
        "#ADD8E6",
        "#98FB98",
        "#20B2AA",
        "#FFF5EE",
        "#FF1493",
        "#6495ED",
        "#8B0000",
        "#D2B48C",
        "#7FFFD4",
        "#FFEFD5",
        "#00008B",
        "#D8BFD8",
        "#FAFAD2",
        "#FFD700",
        "#7FFF00",
        "#00FF7F",
        "#00FFFF",
        "#F5F5DC",
        "#87CEEB",
        "#7B68EE",
        "#ADFF2F",
        "#90EE90",
        "#FF00FF",
        "#708090",
        "#BA55D3",
        "#DCDCDC",
        "#FAEBD7",
        "#FFE4C4",
        "#FFF0F5",
        "#8B4513",
        "#FF7F50",
        "#8A2BE2",
        "#FFC0CB",
        "#00BFFF",
        "#8B008B",
        "#FF00FF",
        "#FFFF00",
        "#CD853F",
        "#B22222",
        "#00FF00",
        "#DDA0DD",
        "#F08080",
        "#008080",
        "#B8860B",
        "#F8F8FF",
        "#808080",
        "#F4A460",
        "#9966CC",
        "#0000FF",
        "#CD5C5C",
        "#00CED1",
        "#F0F8FF",
        "#4B0082",
        "#FF0000",
        "#FFFAFA",
        "#2F4F4F",
        "#FF69B4",
        "#9370DB",
        "#7CFC00",
        "#008000",
        "#BC8F8F",
        "#E0FFFF",
        "#FFE4E1",
        "#FFA500",
        "#3CB371",
        "#1E90FF",
        "#A52A2A",
        "#0000CD",
        "#000080",
        "#FFE4B5",
        "#7B68EE",
        "#DA70D6",
        "#FFA07A",
        "#008B8B",
        "#48D1CC",
        "#2E8B57",
      ]
    }
  },
  customShadows: {
    widgetLight:
      "0px 3px 8px 0px #e8eafc52, 0 3px 3px -2px #B2B2B21A, 0 1px 8px 0 #9A9A9A1A",
    widget:
      "0px 3px 8px 0px #E8EAFC, 0 3px 3px -2px #B2B2B21A, 0 1px 8px 0 #9A9A9A1A",
    widgetDark:
      "0px 3px 18px 0px #4558A3B3, 0 3px 3px -2px #B2B2B21A, 0 1px 8px 0 #9A9A9A1A",
    widgetWide:
      "0px 12px 33px 0px #E8EAFC, 0 3px 3px -2px #B2B2B21A, 0 1px 8px 0 #9A9A9A1A"
  },
  overrides: {
    MuiFormControl: {
      root: {
        marginBottom: "10px"
      }
    },
    MuiButton: {
      containedPrimary: {
        boxShadow: 'none'
      }
    },
    MuiBackdrop: {
      root: {
        backgroundColor: "#4A4A4A1A"
      }
    },
    MuiMenu: {
      paper: {
        boxShadow:
          "0px 3px 11px 0px #E8EAFC, 0 3px 3px -2px #B2B2B21A, 0 1px 8px 0 #9A9A9A1A"
      }
    },
    MuiSelect: {
      icon: {
        color: "#B9B9B9",
      }
    },
    MuiListItem: {
      root: {
        "&$selected": {
          backgroundColor: '#F3F5FF !important',
          '&:focus': {
            backgroundColor: '#F3F5FF',
          },
        }
      },
      button: {
        '&:hover, &:focus': {
          backgroundColor: tinycolor(primary)
            .lighten(mediumLightenRate)
            .toHexString(),
        },
      }
    },
    MuiTouchRipple: {
      child: {
        backgroundColor: "white"
      }
    },
    MuiTableRow: {
      root: {
        height: 0,
        '&:hover': {
          backgroundColor: `${tinycolor(primary)
            .lighten(mediumLightenRate)
            .toHexString()} !important`
        }
      }
    },
    MuiTableCell: {
      root: {
        borderBottom: '1px solid rgba(224, 224, 224, .5)',
      },
      head: {
        fontSize: '0.95rem',
      },
      body: {
        fontSize: '0.95rem',
      }
    },
    MuiSvgIcon: {
      root: {
        width: '1em',
        height: '1em'
      }
    }
  }
};
