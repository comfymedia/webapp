export const DEFAULT_ORDERS = Object.freeze({
  LATEST: 'LATEST',
  TRENDING: 'TRENDING'
});

export const DEFAULT_CATEGORIES = Object.freeze({
  RANDOM: 'RANDOM',
  VERNSFW: 'VERYNSFW',
  FOODIE: 'FOODIE',
  NSFW: 'NSFW',
  SPORTS : 'SPORTS',
  LOOKS: 'LOOKS',
  NEWS: 'NEWS',
  OUTDOOR: 'OUTDOOR'
});

export const DEFAULT_SOURCES = Object.freeze({
  YOUTUBE: 'YOUTUBE',
  REDDIT: 'REDDIT',
  PORNHUB: 'PORNHUB',
  INSTAGRAM: 'INSTAGRAM'
});

export const DEFAULT_MEDIA_TYPES = Object.freeze({
  IMAGE: 'IMAGE',
  GIF: 'GIF',
  VIDEO: 'VIDEO'
});

export class ContentEntry {
  id = null;
  publishedAt = null;
  sourceUrl = null;
  contentUrl = null;
  title = null;
  contentType = null;
  source = null;
  category = null;


  constructor(id, publishedAt, sourceUrl, contentUrl, title, contentType, source, category) {
    this.id = id;
    this.publishedAt = publishedAt;
    this.sourceUrl = sourceUrl;
    this.contentUrl = contentUrl;
    this.title = title;
    this.contentType = contentType;
    this.source = source;
    this.category = category;
  }

  static fromComfyResponse(json) {
    return new ContentEntry(json.id, new Date(json.publishedAt), json.sourceUrl, json.contentUrl, json.title,
      json.contentType, json.source, json.category);
  }
}

export class ContentRequest {
  categories = null;
  order = null;
  size = null;
  start = null;

  constructor(categories, order, size, start) {
    this.categories = categories;
    this.order = order;
    this.size = size;
    this.start = start;
  }
}
