import {ClientError} from "../common/models";
import {getQueryParameters, responseHandler} from "../common/ClientsUtilities";
import {ContentEntry} from "./models";

const API_URL = `${process.env.REACT_APP_API_HOST}/cfy/v1`;

export const pollData = async (request) => {
  if (!request) {
    throw new ClientError(400, 'Missing request data.');
  }

  const requestOptions = {
    method: 'GET',
    headers: {'Content-Type': 'application/json'}
  };

  const parameters = getQueryParameters({
    start: request.start,
    size: request.size,
    categories: request.categories
  });

  const url = `content/${request.order}?${parameters}`;

  const data = await fetch(`${API_URL}/${url}`, requestOptions);
  const result = (await responseHandler(data) || []).map(ContentEntry.fromComfyResponse);
  return result.slice(0, result.length - 1); //remove the last element to avoid duplicates since the last entry will the first of the next poll
};

export const getCategories = async () => {
  const requestOptions = {
    method: 'GET',
    headers: {'Accept': 'application/json'}
  };

  const url = 'categories';

  const response = await fetch(`${API_URL}/${url}`, requestOptions);
  return await responseHandler(response) || [];
};

export const getSources = async () => {
  const requestOptions = {
    method: 'GET',
    headers: {'Accept': 'application/json'}
  };

  const url = 'sources';

  const response = await fetch(`${API_URL}/${url}`, requestOptions);
  return await responseHandler(response) || [];
};

export const getOrders = async () => {
  const requestOptions = {
    method: 'GET',
    headers: {'Accept': 'application/json'}
  };

  const url = 'sort';

  const response = await fetch(`${API_URL}/${url}`, requestOptions);
  return await responseHandler(response) || [];
};
