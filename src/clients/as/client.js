import {Authentication} from './models';
import {ClientError} from '../common/models';
import {getBearerHeader, responseHandler} from '../common/ClientsUtilities';

const API_URL = process.env.REACT_APP_API_HOST;
const API_CLIENT_ID = process.env.REACT_APP_API_CLIENT_ID;

export const login = async (username, password) => {

  if (!username || !password) {
    throw new ClientError(400, 'Invalid data, username or password are not defined.');
  }

  const body = {
    username, password, grant_type: 'password', client_id: API_CLIENT_ID
  };

  const requestOptions = {
    method: 'POST',
    headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'},
    body: _toUrlEncoded(body)
  };

  const data = await fetch(`${API_URL}/as/oauth/token`, requestOptions);
  return Authentication.fromAsResponse(await responseHandler(data));
};

export const logout = async () => {
  const requestOptions = {
    method: 'POST',
    headers: getBearerHeader()
  };

  try {
    await fetch(`${API_URL}/as/oauth/logout`, requestOptions);
  } catch (e) {
    //silently ignore logout error from as
  }
};

export const refresh = async (refreshToken) => {
  if (!refreshToken) {
    throw new ClientError(400, 'Unable to authenticate.');
  }

  const body = {
    refresh_token: refreshToken, grant_type: 'refresh_token', client_id: API_CLIENT_ID
  };

  const requestOptions = {
    method: 'POST',
    headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8'},
    body: _toUrlEncoded(body)
  };

  const data = await fetch(`${API_URL}/as/oauth/token`, requestOptions);
  return Authentication.fromAsResponse(await responseHandler(data));
};

const _toUrlEncoded = (body) => {
  const formBody = [];

  Object.keys(body).forEach(key => {

    const encodedKey = encodeURIComponent(key);
    const encodedValue = encodeURIComponent(body[key]);

    formBody.push(encodedKey + "=" + encodedValue);
  });

  return formBody.join("&");
};
