import {login, logout, refresh} from './client';

export default {
  login,
  logout,
  refresh
};
