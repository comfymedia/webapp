export class Authentication {
  accessToken = null;
  refreshToken = null;
  expiresIn = null;
  user = null;
  clientAuthorities = [];

  constructor(accessToken, refreshToken, expiresIn, user, authorities) {
    this.accessToken = accessToken;
    this.refreshToken = refreshToken;
    this.expiresIn = expiresIn;
    this.user = user;
    this.clientAuthorities = authorities;
  }

  static fromAsResponse(json) {
    return new Authentication(json.access_token, json.refresh_token, json.expires_in, json.user, json.client_authorities);
  }
}
