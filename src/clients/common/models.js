export class ClientError extends Error {
  error = null;
  description = null;
  status = null;

  constructor(error, description, status) {
    super(description);
    this.error = error;
    this.description = description;
    this.status = status;
  }

  static fromResponse(jsonResponse, status) {
    return new ClientError(jsonResponse.error, (jsonResponse.error_description || jsonResponse.message) || 'Unexpected error.', status);
  }

  static fromGraphqlError(graphqlError) {
    return new ClientError(graphqlError.originalError, graphqlError.message || 'Unexpected error.', 500);
  }
}

export class PaginatedResponse {
  page = null;
  totalNumberOfPages = null;
  totalNumberOfElements = null;
  size = null;


  constructor(page, totalNumberOfPages, totalNumberOfElements, size) {
    this.page = page;
    this.totalNumberOfPages = totalNumberOfPages;
    this.totalNumberOfElements = totalNumberOfElements;
    this.size = size;
  }

}
