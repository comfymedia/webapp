import {ClientError} from "./models";

export const responseHandler = async (response) => {
  let data;

  try {
    const text = await response.text();
    data = text && JSON.parse(text);
  } catch (e) {
    return new ClientError('500', 'Unexpected error.');
  }

  if (!response.ok) {
    throw ClientError.fromResponse(data, response.status);
  } else {
    return data;
  }
};

export const getQueryParameters = (obj) => {
  const params = Object.keys(obj)
      .filter(key => obj[key] !== null && obj[key] !== undefined)
      .map(key => `${key}=${Array.isArray(obj[key]) ? obj[key].join(',') : obj[key] }&`)
      .reduce((prev, curr) => prev.concat(curr), '');

  return params.substring(0, params.length - 1);
};

