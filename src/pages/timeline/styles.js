export default theme => ({
  root: {
    flexGrow: 1,
  },
  toolbar: {
    ...theme.mixins.toolbar
  },
  timeline: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    position: 'relative',
    [theme.breakpoints.up("md")]: {
      minWidth: 600
    },
    [theme.breakpoints.down("md")]: {
      width: '100%',
      padding: theme.spacing()
    },
    [theme.breakpoints.down("sm")]: {
      marginTop: 100
    }
  },
  infiniteContainer: {
    overflow: 'auto',
    position: 'relative'
  },
  infiniteLoading: {
    position: 'absolute',
    top: 0,
    left: 0,
    height: '100%',
    width: '100%',
    background: theme.palette.background.watermark,
    zIndex: 1
  },
  mobileAdd: {
    display: 'flex',
    position: 'inherit',
    width: '100%',
    margin: theme.spacing(),
    height: 300,
    top: 0,
    right: 0,
    [theme.breakpoints.between('lg', 'xl')]: {
      display: 'none'
    }
  },
  masonryAdd: {
    display: 'flex !important',
    position: 'inherit',
    width: '100%',
    margin: theme.spacing(),
    height: 300,
    top: 0,
    right: 0
  },
  noDataContainer: {
    display: 'flex',
    position: 'inherit',
    width: '100%',
    margin: theme.spacing(),
    height: 300,
    top: 0,
    right: 0
  },
  masonryGrid: {
    display: 'flex',
    marginLeft: 'auto',
    paddingRight: 8,
    marginTop: 110
  },
  masonryGridColumn: {
    margin: theme.spacing(),
    marginLeft: 0,
    backgroundClip: 'padding-box',
    '& div': { /* change div to reference your elements you put in <Masonry> */
      background: 'white'
    }
  }
});
