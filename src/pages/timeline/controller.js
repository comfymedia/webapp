import React, {useEffect, useRef, useState} from 'react';
import TimelineView from './view';
import ComfyClient, {ContentRequest} from '../../clients/comfy';
import {AlertsActions} from "../../state";
import {useDispatch, useSelector} from 'react-redux';
import throttle from 'lodash.throttle';

function useDidUpdateEffect(fn, inputs) {
  const didMountRef = useRef(false);

  useEffect(() => {
    if (didMountRef.current)
      fn();
    else
      didMountRef.current = true;
  }, inputs);
}

const getRemoteData = async ({
                               size,
                               start,
                               order,
                               categories,
                               contentTypes,
                               sources
                             }) => {
  const request = new ContentRequest(categories, order, size, start);

  return await ComfyClient.pollData(request);
};

const filterData = ({
                      data = [],
                      sources,
                      contentTypes,
                      categories
                    }) => {
  return data.filter(item => sources.includes(item.source) && categories.includes(item.category))
};

const TimelineController = () => {
  const displayNumberOfItems = 10;
  const apiNumberOfItems = 1000;
  const maxRecursiveCalls = 5;

  const dispatch = useDispatch();
  const layoutState = useSelector(state => state.layout);
  const contentState = useSelector(state => state.content);

  const currentCategories = contentState.categories;
  const currentOrder = contentState.order;
  const currentMediaTypes = contentState.mediaTypes;
  const currentSources = contentState.sources;
  const multiply = contentState.multiply;

  const scrollRef = useRef(null);
  const [apiBuffer, setApiBuffer] = useState([]);
  const [displayedItems, setDisplayedItems] = useState(Array.from({length: displayNumberOfItems}, () => ({})));
  const [scrollIndex, setScrollIndex] = useState(0);
  const [isLoading, setIsLoading] = useState(true);
  const [apiIndex, setApiIndex] = useState(0);

  const getFilteredRemoteData = async ({
                                         apiStart
                                       }) => {
    const data = await getRemoteData({
      size: apiNumberOfItems,
      start: apiStart,
      order: currentOrder,
      categories: currentCategories,
      contentTypes: currentMediaTypes,
      sources: currentSources
    });

    return filterData({
      data,
      categories: currentCategories,
      contentTypes: currentMediaTypes,
      sources: currentSources
    });
  };

  const recursiveFetcher = async ({
                                    apiStart = 0,
                                    recursiveCalls = 0,
                                    accumulatedData = []
                                  }) => {
    if (recursiveCalls === maxRecursiveCalls || accumulatedData.length >= displayNumberOfItems) {
      return {apiStart, recursiveCalls, accumulatedData};
    } else {
      const data = await getFilteredRemoteData({apiStart});

      const returning = {
        apiStart: apiStart + apiNumberOfItems,
        recursiveCalls: recursiveCalls + 1,
        accumulatedData: [...accumulatedData, ...data]
      };

      if (data.length < displayNumberOfItems) {
        return await recursiveFetcher(returning);
      } else {
        return returning;
      }
    }
  };

  const refreshStateData = () => {
    return recursiveFetcher({
      apiStart: 0
    })
      .then(data => {
          const {apiStart, accumulatedData, recursiveCalls} = data;

          setDisplayedItems(accumulatedData.slice(0, displayNumberOfItems));
          setApiBuffer(accumulatedData);
          setScrollIndex(displayNumberOfItems);
          setApiIndex(apiStart);
        }
      )
      .catch(e => AlertsActions.addErrorAlert(e.description)(dispatch))
      .finally(() => setIsLoading(false));
  };

  //on mount effect
  useEffect(() => {
    refreshStateData().finally();
  }, []);

  //on filters change effect
  useDidUpdateEffect(() => {
    setIsLoading(true);

    scrollRef.current.el.scrollTo({top: 0});

    refreshStateData().finally();
  }, [currentCategories, currentOrder, currentSources, currentMediaTypes]);

  //handlers
  const handleLoad = throttle(async () => {
    const nextIndex = scrollIndex + displayNumberOfItems;

    const filteredBufferedItems = filterData({
      data: apiBuffer,
      contentTypes: currentMediaTypes,
      categories: currentCategories,
      sources: currentSources
    });

    const toDisplayItems = filteredBufferedItems.slice(scrollIndex, nextIndex);

    if (toDisplayItems.length < displayNumberOfItems) {
      const newData = await recursiveFetcher({
        apiStart: apiIndex,
        recursiveCalls: 1,
        accumulatedData: toDisplayItems
      });

      const {apiStart, accumulatedData, recursiveCalls} = newData;
      const toAppendData = accumulatedData.slice(0, displayNumberOfItems);
      setDisplayedItems([...displayedItems, ...toAppendData]);
      setApiBuffer(accumulatedData);
      setScrollIndex(nextIndex);
      setApiIndex(apiStart);
    } else {
      setDisplayedItems([...displayedItems, ...toDisplayItems]);
      setScrollIndex(nextIndex);
    }
  }, 1000);

  const handleRefresh = (data) => {
    return data;
  };

  return <TimelineView
    items={displayedItems}
    isLoading={isLoading}
    handleLoad={handleLoad}
    handleRefresh={handleRefresh}
    ref={scrollRef}
    multiply={multiply}
    isSidebarOpen={layoutState.isSidebarOpened}
  />
};

export default TimelineController;
