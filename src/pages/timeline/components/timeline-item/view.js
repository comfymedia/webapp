import React from 'react';
import {
  Card,
  CardActions,
  CardContent,
  Chip,
  Fade,
  IconButton,
  Paper,
  Popper,
  Typography,
  withStyles
} from "@material-ui/core";
import VisibilitySensor from 'react-visibility-sensor';
import styles from './styles';
import CustomLoadingBar from "../../../../components/LoadingBar/custom-loading-bar";
import {FileCopyRounded as CopyIcon, OpenInNew as OpenInNewIcon} from "@material-ui/icons";

const TimelineItemView = ({
                            classes,
                            item,
                            embedlyKey,
                            handleEmbedlyInit,
                            isLoading,
                            isEmbeddedContent,
                            isGif,
                            handleCopyToClipboard,
                            copyAreaRef,
                            popperOpen,
                            popperAnchor,
                            handleOpenSource,
                            handleVisibilityChange,
                            isVisible
                          }) => {
  const container = document.getElementById('infinite-container');

  return container ? <VisibilitySensor
    partialVisibility={true}
    scrollCheck={true}
    delayedCall={true}
    containment={container}
    onChange={handleVisibilityChange}>
    <Card className={classes.root}
          raised={true}
          style={{
            height: isEmbeddedContent ? 600 : 'auto',
            visibility: isVisible ? 'visible' : 'hidden'
          }}>
      <CardContent className={classes.content}>
        {
          isLoading ? <CustomLoadingBar/> : null
        }
        {
          isEmbeddedContent ?
            <embed src={item.contentUrl}
                   className={classes.embed}
            />
            :
            <a className="embedly-card"
               href={isGif ? item.contentUrl : item.contentUrl}
               data-card-controls={0}
               data-card-key={embedlyKey}
               ref={handleEmbedlyInit}
            >
              <span/>
            </a>
        }
      </CardContent>
      <CardActions className={classes.actionsRoot}>
        <h1 className={classes.h1Invisible}>
          MEDIA
        </h1>
        <h1 className={classes.h1Invisible}>
          COMFY
        </h1>
        <h1 className={classes.h1Invisible}>
          {item.category}
        </h1>
        <h1 className={classes.h1Invisible}>
          {item.source}
        </h1>
        <Chip
          component={'div'}
          avatar={null}
          label={item.category}
          className={classes.customChip}
          variant="outlined"
          color="primary"
        />
        <Chip
          component={'div'}
          avatar={null}
          label={item.source}
          className={classes.customChip}
          variant="outlined"
          color="primary"
        />
        <Chip
          component={'div'}
          avatar={null}
          label={item.contentType}
          className={classes.customChip}
          variant="outlined"
          color="primary"
        />
        <Popper open={popperOpen} anchorEl={popperAnchor} placement={"top"} transition>
          {({TransitionProps}) => (
            <Fade {...TransitionProps} timeout={500}>
              <Paper style={{
                width: 70,
                height: 50,
                alignItems: 'center',
                justifyContent: 'center',
                display: 'flex'
              }} elevation={24}>
                <Typography className={classes.typography}>Copied</Typography>
              </Paper>
            </Fade>
          )}
        </Popper>
        <IconButton
          color="primary"
          onClick={handleCopyToClipboard}
        >
          <CopyIcon/>
        </IconButton>
        <IconButton
          color="primary"
          onClick={() => handleOpenSource(item.sourceUrl)}
        >
          <OpenInNewIcon/>
        </IconButton>
        <textarea
          readOnly={true}
          ref={copyAreaRef}
          style={{
            height: 0,
            width: 0,
            padding: 0,
            margin: 0,
            zIndex: -1
          }}
          value={item.contentUrl}
        />
      </CardActions>
    </Card>
  </VisibilitySensor> : null;
};

export default withStyles(styles, {withTheme: true})(TimelineItemView);
