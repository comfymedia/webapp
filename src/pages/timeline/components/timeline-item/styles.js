export default theme => ({
  root: {
    margin: theme.spacing(),
    position: 'relative',
    display: 'flex',
    flexDirection: 'column',
    marginTop: theme.spacing(2),
    borderRadius: 5,
    width: '100%'
  },
  content: {
    width: '100%',
    height: '100%',
    minHeight: 300,
    [theme.breakpoints.down("sm")]: {
      minWidth: '90vw',
    },
  },
  actionsRoot: {
    display: 'flex',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    flexWrap: 'wrap',
    flexDirection: 'row',
    padding: theme.spacing(),
  },
  embed: {
    width: '100%',
    height: '100%'
  },
  embeddedContent: {
    height: 300
  },
  customChip: {
    margin: theme.spacing(),
    padding: theme.spacing(),
    borderRadius: 5
  },
  h1Invisible: {
    visibility: 'hidden',
    position: 'absolute',
    top: 0
  }
});
