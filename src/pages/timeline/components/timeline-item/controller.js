import React, {useEffect, useRef, useState} from 'react';
import TimelineItemView from './view';
import {DEFAULT_SOURCES} from "../../../../clients/comfy";
import {DEFAULT_MEDIA_TYPES} from "../../../../clients/comfy/models";
import {useSelector} from "react-redux";

const EMBEDLY_API_KEY = '491b51795280480687d3ffe5d11bd934';

const TimelineItemController = ({
                                  item,
                                  isLoadingOverride
                                }) => {
  //if its embedded content we won't be using embedly, so we use parent/request loading
  const isEmbeddedContent = item.source === DEFAULT_SOURCES.YOUTUBE || item.source === DEFAULT_SOURCES.PORNHUB || (item.source === DEFAULT_SOURCES.CHAN && item.contentType === DEFAULT_MEDIA_TYPES.VIDEO);
  const isGif = item.contentType === DEFAULT_MEDIA_TYPES.GIF;

  const contentState = useSelector(state => state.content);

  const [isLoading, setIsLoading] = useState(true);
  const [popperOpen, setPopperOpen] = useState(false);
  const [embedly, setEmbedly] = useState(null);
  const [popperAnchor, setPopperAnchor] = React.useState(null);
  const [isVisible, setIsVisible] = React.useState(true);
  const copyAreaRef = useRef(null);

  //handlers
  const handleCopyToClipboard = (e) => {
    setPopperOpen(true);
    setPopperAnchor(e.currentTarget);

    copyAreaRef.current.select();
    document.execCommand('copy');
    setTimeout(() => {
      setPopperOpen(false);
    }, 500);
  };

  //effects
  useEffect(() => {
    if (embedly) {
      embedly.on('card.rendered', function (iframe) {
        setIsLoading(false);
      });
    }
  }, [embedly]);

  //handlers
  const handleEmbedlyInit = (el) => {
    setEmbedly(window.embedly('card', el));
  };

  const handleOpenSource = (url) => {
    window.open(url, '_blank');
  };

  const handleVisibilityChange = (isVisible) => {
    setIsVisible(isVisible);
  };

  return <TimelineItemView
    item={item}
    embedlyKey={EMBEDLY_API_KEY}
    isLoading={isEmbeddedContent ? isLoadingOverride : isLoading}
    handleEmbedlyInit={handleEmbedlyInit}
    isEmbeddedContent={isEmbeddedContent}
    isGif={isGif}
    handleCopyToClipboard={handleCopyToClipboard}
    copyAreaRef={copyAreaRef}
    multiply={contentState.multiply}
    popperOpen={popperOpen}
    popperAnchor={popperAnchor}
    handleOpenSource={handleOpenSource}
    isVisible={isVisible}
    handleVisibilityChange={handleVisibilityChange}
  />
};

export default TimelineItemController;
