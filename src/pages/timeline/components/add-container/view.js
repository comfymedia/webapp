import React from 'react';
import {withStyles} from "@material-ui/core";
import styles from './styles';

const AddContainerView = ({
                            classes,
                            addId
                          }) => {
  return <div className={classes.root}>
    {
      <div id={addId}/>
    }
  </div>;
};

export default withStyles(styles, {withTheme: true})(AddContainerView);
