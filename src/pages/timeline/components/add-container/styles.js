export default theme => ({
  root: {
    //border: `1px solid ${theme.palette.primary.mediumLight}`,
    marginLeft: theme.spacing(),
    height: '90vh',
    width: 300,
    position: 'fixed',
    top: 80,
    right: 10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
    background: 'white'
  }
});
