import React, {forwardRef} from 'react';
import {makeStyles, useMediaQuery, withStyles} from '@material-ui/core';
import {TimelineItem} from "./components/timeline-item";
import styles from './styles';
import InfiniteScroll from 'react-infinite-scroll-component';
import {Info as InfoIcon} from "@material-ui/icons";
import clsx from "clsx";
import {AddContainer} from "./components/add-container";
import Masonry from "react-masonry-css";

const rightAddId = 'awn-z2753995';
const topMobileAddId = 'awn-z2755099';

const RenderMasonry = withStyles(styles)(({
                                              classes,
                                              items,
                                              isLoading,
                                              isSidebarOpen
                                          }) => {
    return <React.Fragment>
        <Masonry
            breakpointCols={2}
            className={classes.masonryGrid}
            style={{
                width: isSidebarOpen ? 'calc(100vw - 300px)' : '100%',
            }}
            columnClassName={classes.masonryGridColumn}>
            {
                items.map((item, index) => {
                    //const shouldDisplayAdd = index % 3 === 0;
                    const shouldDisplayAdd = false;
                    return <React.Fragment key={item.id || index}>
                        <TimelineItem isLoadingOverride={isLoading} item={item}/>
                        {
                            shouldDisplayAdd ? <AddContainer
                                classes={{
                                    root: classes.masonryAdd
                                }}
                            /> : null
                        }
                    </React.Fragment>;
                })
            }
        </Masonry>
    </React.Fragment>
});

const RenderSingleColumn = withStyles(styles)(({
                                                   classes,
                                                   isLoading,
                                                   items
                                               }) => {
    return <React.Fragment>
        <div className={classes.timeline}>
            {
                isLoading ? <div className={classes.infiniteLoading}/> : null
            }
            {
                items.map((item, index) => {
                    //const shouldDisplayAdd = index % 3 === 0;
                    const shouldDisplayAdd = false;
                    return <React.Fragment key={item.id || index}>
                        <TimelineItem isLoadingOverride={isLoading} item={item}/>
                        {
                            shouldDisplayAdd ? <AddContainer
                                classes={{
                                    root: classes.mobileAdd
                                }}
                            /> : null
                        }
                    </React.Fragment>;
                })
            }
        </div>
    </React.Fragment>;
});

const TimelineView = forwardRef(({
                                     classes,
                                     theme,
                                     items,
                                     handleLoad,
                                     handleRefresh,
                                     isLoading,
                                     multiply,
                                     isSidebarOpen
                                 }, ref) => {
    multiply = false; // don't use masonry for now.

    const isSmallScreen = useMediaQuery(theme.breakpoints.down("sm"));
    const isMasonry = multiply && !isSmallScreen;

    const useStyles = makeStyles(theme => ({
        rightAd: {
            zIndex: !isMasonry ? theme.zIndex.drawer - 1 : -1,
            [theme.breakpoints.down("md")]: {
                zIndex: -1,
            },
            height: '100%',
            '& div': {
                height: '100%',
            },
            visibility: 'hidden'
        },
        mobileTopAd: {
            width: '100%',
            height: 120,
            top: 60,
            zIndex: theme.zIndex.drawer - 1,
            [theme.breakpoints.up("md")]: {
                display: 'none',
            },
            right: 0,
            visibility: 'hidden'
        }
    }));

    const customStyles = useStyles();

    return (<div className={classes.root}>
        <div className={classes.toolbar}/>
        <div className={classes.infiniteContainer} id={"infinite-container"}>
            {/*right banner*/}
            <AddContainer
                addId={rightAddId}
                classes={{
                    root: customStyles.rightAd
                }}
            />

            {/*mobile top banner*/}
            <AddContainer addId={topMobileAddId}
                          classes={{
                              root: customStyles.mobileTopAd
                          }}
            />

            <InfiniteScroll
                ref={ref}
                height={window.innerHeight}
                dataLength={items.length}
                loader={null}
                next={handleLoad}
                initialScrollY={0}
                refreshFunction={handleRefresh}
                endMessage={
                    <p style={{textAlign: 'center'}}>
                        <InfoIcon className={clsx(classes.icon, classes.iconVariant)}/>
                        The End
                    </p>
                }
                hasMore={true}
                hasChildren={true}
                scrollThreshold={0.8}
            >
                <div style={{
                    display: 'flex',
                    justifyContent: 'center',
                    alignContent: 'center',
                    flexDirection: 'row',
                    position: 'relative'
                }}>
                    {
                        isMasonry ?
                            <RenderMasonry
                                items={items}
                                isLoading={isLoading}
                                isSidebarOpen={isSidebarOpen}
                                isMasonry={isMasonry}
                            />
                            : <RenderSingleColumn
                                items={items}
                                isLoading={isLoading}
                                isMasonry={isMasonry}
                            />
                    }
                </div>
            </InfiniteScroll>
        </div>
    </div>);
});

export default withStyles(styles, {withTheme: true})(TimelineView);
