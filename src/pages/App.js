import React from 'react';
import {BrowserRouter, Redirect, Route, Switch} from 'react-router-dom';
import {createMuiTheme, MuiThemeProvider} from '@material-ui/core/styles';

import themes, {overrides} from '../themes';
import {CssBaseline} from "@material-ui/core";

import Layout from '../components/Layout';
import Error from './error';

const theme = createMuiTheme({...themes.default, ...overrides});

const PublicRoute = ({component, isAuthenticated, redirectToDashboard = true, ...rest}) => {
    return (
        <Route
            {...rest} render={props => (
            isAuthenticated && redirectToDashboard ? (
                <Redirect
                    to={{
                        pathname: '/',
                    }}
                />
            ) : (
                React.createElement(component, props)
            )
        )}
        />
    );
};

const App = () => {
    return (
        <MuiThemeProvider theme={theme}>
            <CssBaseline/>
            <BrowserRouter>
                <Switch>
                    <Route exact path="/" render={() => <Redirect to="/grid"/>}/>

                    <PublicRoute path="/grid" component={Layout}/>

                    <Route component={Error}/>
                </Switch>
            </BrowserRouter>
        </MuiThemeProvider>
    );
};

export default App;
