import {combineReducers} from 'redux';
import {AlertsReducer, ContentReducer, LayoutReducer} from '../state';

const appReducer = combineReducers({
  alerts: AlertsReducer,
  layout: LayoutReducer,
  content: ContentReducer
});

const rootReducer = (state, action) => {
  return appReducer(state, action)
};

export default rootReducer;
