import React from 'react';
import {useDispatch, useSelector} from 'react-redux';

import LayoutView from './view';
import {AlertsActions} from "../../state";

const LayoutController = () => {

  const alertsState = useSelector(state => state.alerts);
  const dispatch = useDispatch();

  const handleNotificationClose = (e, reason) => {
    AlertsActions.resetAlert()(dispatch);
  };

  return <LayoutView
    alertsState={alertsState}
    handleNotificationClose={handleNotificationClose}
  />
};

export default LayoutController;
