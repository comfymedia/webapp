import React, {useRef} from 'react';
import {CssBaseline, Fade, withStyles} from '@material-ui/core';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import styles from "./styles";

import Grid from '../../pages/timeline';
import {CustomSnackBar} from "../Alerts";
import Header from "../Header";
import Sidebar from "../Sidebar";

const Layout = ({
                  classes,
                  alertsState,
                  handleNotificationClose
                }) => {
  const headerRef = useRef(null);

  return (
    <div className={classes.root}>
      <CssBaseline/>
      <BrowserRouter>
        <React.Fragment>
          <Header ref={headerRef}/>
          <Sidebar headerRef={headerRef}/>
          <div className={classes.content}>
            <Fade in={alertsState.open}>
              <CustomSnackBar
                overrides={{
                  anchorOriginTopCenter: {
                    top: '5% !important'
                  }
                }}
                anchorOrigin={{
                  horizontal: 'center', vertical: 'top'
                }}
                onClose={handleNotificationClose}
                variant={alertsState.variant}
                message={alertsState.message}
                open={alertsState.open}
              />
            </Fade>
            <Switch>
              <Route path="/grid" component={Grid}/>
            </Switch>
          </div>
        </React.Fragment>
      </BrowserRouter>
    </div>
  )
};

export default withStyles(styles, {withTheme: true})(Layout);
