export default theme => ({
  root: {
    display: 'flex',
    maxWidth: '100vw',
    overflowX: 'hidden'
  },
  content: {
    flexGrow: 1,
    width: `calc(100vw - 240px)`,
    minHeight: '100vh',
    backgroundColor: theme.palette.background.shaded,
    position: 'relative'
  },
  fakeToolbar: {
    ...theme.mixins.toolbar,
  },
  overlay: {
    position: 'absolute'
  },
  overlayShown: {
    height: '100%',
    width: '100%',
    zIndex: theme.zIndex.drawer - 1,
    background: theme.palette.background.overlay,
    top: -1,
    left: -1
  }
});
