export default theme => ({
  logo: {
    color: "white",
    height: 45,
    paddingRight: theme.spacing(),
    paddingLeft: theme.spacing()
  },
  appBar: {
    width: "100vw",
    height: 60,
    justifyContent: 'center',
    zIndex: theme.zIndex.drawer + 1,
    background: theme.palette.primary.main,
    transition: theme.transitions.create(["margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    })
  },
  toolbar: {
    padding: 0
  },
  headerMenu: {
    marginTop: theme.spacing(6)
  },
  headerRightMenu: {
    marginRight: theme.spacing(2)
  },
  headerMenuButton: {
    padding: theme.spacing() / 2,
    backgroundColor: theme.palette.primary.mediumLight,
    "&:hover, &:focus": {
      backgroundColor: theme.palette.primary.mediumLight
    }
  },
  headerMenuButtonCollapse: {
    marginRight: theme.spacing(2)
  },
  headerIcon: {
    fontSize: 28,
    color: "rgba(255, 255, 255, 0.35)"
  },
  headerIconCollapse: {
    color: "white"
  },
  headerNotificationBadgeRoot: {
    color: theme.palette.warning.main
  },
  multiplyButton: {

  }
});
