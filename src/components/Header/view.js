import React, {forwardRef} from "react";
import {AppBar, IconButton, Toolbar, useMediaQuery, withStyles} from "@material-ui/core";
import {
  ChevronLeft as ChevronLeftIcon,
  HorizontalSplit as ViewSingleColumnIcon,
  Menu as MenuIcon,
  VerticalSplit as VerticalSplitIcon
} from "@material-ui/icons";
import classNames from "classnames";

import styles from './styles';
import logo from '../../images/logo.png';

const RenderMultiplySwitch = withStyles(styles, {withTheme: true})(({
                                                                      theme,
                                                                      classes,
                                                                      multiply,
                                                                      handleMultiplyChange
                                                                    }) => {
  return (
    <IconButton
      id={"multiply-button"}
      onClick={handleMultiplyChange}
      className={classNames(
        classes.headerMenuButton,
        classes.headerMenuButtonCollapse
      )}
      disableRipple={true}
      disableFocusRipple={true}>
      {
        multiply ?
          <ViewSingleColumnIcon
            classes={{
              root: classNames(classes.headerIcon, classes.headerIconCollapse)
            }}/> : <VerticalSplitIcon
            classes={{
              root: classNames(classes.headerIcon, classes.headerIconCollapse)
            }}/>
      }
    </IconButton>
  );
});

const RenderSideBarToggle = withStyles(styles)(({
                                                  classes,
                                                  toggleSidebar,
                                                  isSidebarOpened
                                                }) => (
  <IconButton
    color="inherit"
    className={classNames(
      classes.headerMenuButton,
      classes.headerMenuButtonCollapse
    )}
    onClick={toggleSidebar}
    disableRipple={true}
    disableFocusRipple={true}
  >
    {isSidebarOpened ? (
      <ChevronLeftIcon
        classes={{
          root: classNames(classes.headerIcon, classes.headerIconCollapse)
        }}
      />
    ) : (
      <MenuIcon
        classes={{
          root: classNames(classes.headerIcon, classes.headerIconCollapse)
        }}
      />
    )}
  </IconButton>
));

const Header = forwardRef(({
                             classes,
                             theme,
                             isSidebarOpened,
                             toggleSidebar,
                             multiply,
                             handleMultiplyChange
                           }, ref) => {
  const isSmallScreen = useMediaQuery(theme.breakpoints.down("sm"));
  return (<AppBar ref={ref} position="fixed" className={classes.appBar}>
    <Toolbar className={classes.toolbar}>
      <div style={{
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center'
      }}>
        <img src={logo} alt="logo" className={classes.logo}/>
        <div style={{
          borderRight: `1px solid ${theme.palette.secondary.main}`,
          marginLeft: theme.spacing(),
          height: 20,
          width: 1,
          marginRight: theme.spacing(2)
        }}/>
      </div>
      <RenderSideBarToggle toggleSidebar={toggleSidebar} isSidebarOpened={isSidebarOpened}/>
      {/*!isSmallScreen ? <RenderMultiplySwitch multiply={multiply} handleMultiplyChange={handleMultiplyChange}/> : null*/}
    </Toolbar>
  </AppBar>);
});

export default withStyles(styles, {withTheme: true})(Header);
