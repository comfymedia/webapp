import React, {forwardRef} from 'react';
import {useDispatch, useSelector} from 'react-redux';

import HeaderView from './view';
import {ContentActions, LayoutActions} from "../../state";

const Header = forwardRef((props, ref) => {
  //state
  const dispatch = useDispatch();

  const layoutState = useSelector(state => state.layout);
  const contentState = useSelector(state => state.content);
  const multiply = contentState.multiply;

  //handlers
  const toggleSidebar = () => {
    dispatch(LayoutActions.setSidebarOpen(!layoutState.isSidebarOpened));
  };

  const handleMultiplyChange = () => {
    ContentActions.changeMultiply(!multiply)(dispatch);
  };

  return (<HeaderView
    isSidebarOpened={layoutState.isSidebarOpened}
    toggleSidebar={toggleSidebar}
    ref={ref}
    handleMultiplyChange={handleMultiplyChange}
    multiply={multiply}
  />)
});

export default Header;
