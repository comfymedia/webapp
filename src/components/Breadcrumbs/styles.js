export default theme => ({
  breadCrumb: {
    marginBottom: theme.spacing(),
    color: theme.palette.primary.dark
  },
  link: {
    display: 'flex',
    textDecoration: "underline"
  },
  icon: {
    marginRight: theme.spacing(1),
    width: 24,
    height: 24,
    color: theme.palette.primary.main
  },
  lastItem: {
    textDecoration: 'none',
    color: theme.palette.text.primary
  }
});
