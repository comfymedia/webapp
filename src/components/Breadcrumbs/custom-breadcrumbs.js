import {Breadcrumbs, Link, Typography} from "@material-ui/core";
import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import classnames from 'classnames';
import styles from './styles';

const CustomBreadCrumbs = ({
                             classes,
                             data,
                             handler
                           }) => {

  const path = data.slice(0, data.length - 1);
  const last = data[data.length - 1];

  return (
    <Breadcrumbs aria-label="breadcrumb" className={classes.breadCrumb}>
      {
        path.map((p, index) => (<Link
          color="inherit"
          href="#"
          underline="always"
          onClick={(e) => {
            e.preventDefault();
            handler(p.url);
          }}
          className={classes.link}
          key={`bread-path-${index}`}
        >
          {p.icon && <div className={classes.icon}>
            {p.icon}
          </div>}
          {p.text}
        </Link>))
      }
      <Typography
        className={classnames(classes.link, classes.lastItem)}>
        {last.icon && <span className={classes.icon}>
          {last.icon}
        </span>}
        {last.text}
      </Typography>
    </Breadcrumbs>
  )
};

export default withStyles(styles, {withTheme: true})(CustomBreadCrumbs);
