export default theme => ({
  menuButton: {
    marginLeft: 12,
    marginRight: 36,
  },
  hide: {
    display: 'none',
  },
  drawer: {
    position: 'absolute',
    width: theme.drawer.drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
    background: 'white'
  },
  drawerOpen: {
    width: theme.drawer.drawerWidth,
    [theme.breakpoints.down("md")]: {
      width: '80vw',
    },
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
    zIndex: theme.zIndex.drawer,
    background: theme.palette.primary.extraLight
  },
  drawerClose: {
    background: theme.palette.primary.extraLight,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: 0 //theme.spacing(4) + 40
  },
  toolbar: {
    ...theme.mixins.toolbar,
    minHeight: '60px !important',
    [theme.breakpoints.down("sm")]: {
      display: 'none',
    }
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  mobileBackButton: {
    marginTop: theme.spacing(.5),
    marginLeft: theme.spacing(3),
    [theme.breakpoints.only("sm")]: {
      marginTop: theme.spacing(.625),
    },
    [theme.breakpoints.up("md")]: {
      display: 'none',
    }
  },
  sidebarComponentContent: {
    width: '100%',
    backgroundColor: 'white'
  },
  customChip: {
    margin: theme.spacing(),
    padding: theme.spacing(),
    borderRadius: 5
  }
});
