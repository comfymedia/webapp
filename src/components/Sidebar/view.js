import React, {useMemo} from 'react';
import {Chip, ClickAwayListener, Drawer, IconButton, withStyles} from "@material-ui/core";
import {ArrowBack as ArrowBackIcon} from "@material-ui/icons";
import classNames from 'classnames';
import styles from "./styles";

import SideBarComponent from './components/SidebarComponent';

const SidebarView = ({
                       classes,
                       theme,
                       setSidebarOpen,
                       isSidebarOpened,
                       isPermanent,
                       wrapperRef,
                       currentExpanded,
                       sections,
                       handlePanelExpansion,
                       handleSectionComponentItemClick
                     }) => {
  return useMemo(() => (
    <ClickAwayListener onClickAway={setSidebarOpen}>
      <Drawer
        ref={wrapperRef}
        variant={isPermanent ? 'permanent' : 'temporary'}
        className={classNames(classes.drawer, {
          [classes.drawerOpen]: isSidebarOpened,
          [classes.drawerClose]: !isSidebarOpened,
        })}
        classes={{
          paper: classNames({
            [classes.drawerOpen]: isSidebarOpened,
            [classes.drawerClose]: !isSidebarOpened,
          }),
        }}
        open={isSidebarOpened}
      >
        <div className={classes.toolbar}/>
        <div className={classes.mobileBackButton}>
          <IconButton
            onClick={setSidebarOpen}
          >
            <ArrowBackIcon classes={{root: classNames(classes.headerIcon, classes.headerIconCollapse)}}/>
          </IconButton>
        </div>
        {
          sections.map(section => {
            return <SideBarComponent
              key={section.name}
              expanded={currentExpanded.includes(section.name)}
              value={section.name}
              handleOnChange={handlePanelExpansion}
              title={section.name}
              content={
                section.data.map((dataItem, index) => (<Chip
                  id={`filter-${dataItem.label}`}
                  component={'div'}
                  key={index}
                  avatar={null}
                  label={dataItem.label}
                  onClick={() => handleSectionComponentItemClick({
                    sectionName: section.name,
                    dataItem,
                    isChecked: section.isChecked(dataItem.label)
                  })}
                  className={classes.customChip}
                  variant="outlined"
                  color={section.isChecked(dataItem.label) ? 'primary' : 'default'}
                  style={{
                    background: section.isChecked(dataItem.label) ? theme.palette.primary.extraLight : 'inherit'
                  }}
                />))
              }
            />
          })
        }
      </Drawer>
    </ClickAwayListener>), [
    classes,
    setSidebarOpen,
    isSidebarOpened,
    isPermanent,
    wrapperRef,
    currentExpanded,
    sections,
    handlePanelExpansion,
    handleSectionComponentItemClick
  ]);
};
export default withStyles(styles, {withTheme: true})(SidebarView);
