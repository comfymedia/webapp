export default theme => ({
  expansionPanelRoot: {
    backgroundColor: 'white',
    boxShadow: 'none',
    '&:before': {
      display: 'none',
    }
  },
  expansionPanelExpanded: {
    backgroundColor: 'white',
    margin: '0 !important',
    marginBottom: '1px !important',
    display: 'flex',
    flexDirection: 'column',
    flex: 1
  },
  expansionPanelSummaryRoot: {
    backgroundColor: 'white',
    height: '62px !important',
    minHeight: '62px !important',
    padding: '0 16px 0 16px',
    "&:hover": {
      backgroundColor: theme.palette.primary.mediumLight
    }
  },
  expansionPanelSummaryContent: {
    '&$expanded': {
      margin: '12px 0',
    },
    '& p': {
      color: `${theme.palette.text.primary} !important`,
      fontWeight: '550 !important',
    }
  },
  expansionPanelSummaryExpanded: {},
  expansionPanelDetailsRoot: {
    padding: 0,
    width: theme.drawer.drawerWidth,
    whiteSpace: 'initial',
    display: 'flex',
    alignItems: 'center',
    flexWrap: 'wrap',
    justifyContent: 'center'
  }
});
