import React from "react";
import {ExpansionPanel, ExpansionPanelDetails, ExpansionPanelSummary, Typography, withStyles} from "@material-ui/core";
import {ExpandMore as ExpandMoreIcon} from "@material-ui/icons";
import styles from './styles';

const SideBarComponent = ({
                            classes,
                            value,
                            expanded,
                            handleOnChange,
                            title,
                            content
                          }) => {
  return (
    <React.Fragment>
      <ExpansionPanel
        square
        expanded={expanded}
        onChange={() => handleOnChange(value)}
        classes={{
          root: classes.expansionPanelRoot,
          expanded: classes.expansionPanelExpanded
        }}>
        <ExpansionPanelSummary
          expandIcon={<ExpandMoreIcon/>}
          classes={{
            root: classes.expansionPanelSummaryRoot,
            content: classes.expansionPanelSummaryContent,
            expanded: classes.expansionPanelSummaryExpanded
          }}
          id={`${value}-summary-id`}
          aria-controls={`${value}-summary-aria-controls`}
        >
          <Typography className={classes.heading}>{title}</Typography>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails
          classes={{
            root: classes.expansionPanelDetailsRoot
          }}
        >
          {
            content
          }
        </ExpansionPanelDetails>
      </ExpansionPanel>
    </React.Fragment>
  );
};
export default withStyles(styles, {withTheme: true})(SideBarComponent);
