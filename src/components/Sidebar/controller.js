import {withRouter} from 'react-router-dom';
import SidebarView from './view';
import React, {useEffect, useMemo, useRef, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {useTheme} from "@material-ui/core";
import {
  Category as CategoryIcon,
  PermDeviceInformation as SourceIcon,
  PlayCircleFilled as MediaTypeIcon,
  Sort as SortIcon
} from '@material-ui/icons';

import {AlertsActions, ContentActions, LayoutActions} from '../../state';
import ComfyClient from '../../clients/comfy';
import {IS_WIDE_SCREEN} from "../../state/layout/reducers";
import {DEFAULT_MEDIA_TYPES} from "../../clients/comfy/models";

const CATEGORY_SECTION_NAME = 'Category';
const ORDERS_SECTION_NAME = 'Orders';
const SOURCES_SECTION_NAME = 'Sources';
const MEDIA_TYPE_SECTION_NAME = 'MediaType';

const buildSections = ({
                         categories = [],
                         orders = [],
                         sources = [],
                         mediaTypes = [],
                         currentCategories = [],
                         currentOrder,
                         currentSources = [],
                         currentMediaTypes = []
                       }) => {
  return [
    {
      name: CATEGORY_SECTION_NAME,
      data: categories.map(category => ({
        icon: <CategoryIcon/>,
        label: category
      })),
      isChecked: (value) => currentCategories.includes(value)
    },
    {
      name: ORDERS_SECTION_NAME,
      isExclusive: true,
      data: orders.map(order => ({
        icon: <SortIcon/>,
        label: order
      })),
      isChecked: (value) => value === currentOrder
    },
    {
      name: SOURCES_SECTION_NAME,
      data: sources.map(source => ({
        icon: <SourceIcon/>,
        label: source
      })),
      isChecked: (value) => currentSources.includes(value)
    }
  ]
};

const SideBarController = ({headerRef}) => {
  const dispatch = useDispatch();
  const theme = useTheme();
  const wrapperRef = useRef(null);

  const layoutState = useSelector(state => state.layout);
  const contentState = useSelector(state => state.content);

  const currentCategories = contentState.categories;
  const currentOrder = contentState.order;
  const currentSources = contentState.sources;
  const currentMediaTypes = contentState.mediaTypes;

  //state
  const [isPermanent, setPermanent] = useState(true);
  const [categories, setCategories] = useState([]);
  const [orders, setOrders] = useState([]);
  const [sources, setSources] = useState([]);
  const [mediaTypes, setMediaTypes] = useState([]);
  const [currentExpanded, setCurrentExpanded] = useState([CATEGORY_SECTION_NAME, ORDERS_SECTION_NAME, SOURCES_SECTION_NAME, MEDIA_TYPE_SECTION_NAME]);
  const [sections, setSections] = useState(buildSections({
    categories,
    orders,
    sources,
    mediaTypes,
    currentCategories,
    currentOrder,
    currentSources,
    currentMediaTypes
  }));

  //effects
  useEffect(() => {
    Promise.all([
      ComfyClient.getCategories(),
      ComfyClient.getOrders(),
      ComfyClient.getSources(),
      Promise.resolve(Object.values(DEFAULT_MEDIA_TYPES))
    ])
      .then(values => {
        const categories = values[0];
        const orders = values[1];
        const sources = values[2];
        const mediaTypes = values[3];
        setCategories(categories);
        setOrders(orders);
        setSources(sources);
        setMediaTypes(mediaTypes);
        setSections(buildSections({
          categories,
          orders,
          sources,
          mediaTypes,
          currentCategories,
          currentOrder,
          currentSources,
          currentMediaTypes
        }));
      })
      .catch(e => AlertsActions.addErrorAlert(e.description)(dispatch))
      .finally()
  }, []);

  useEffect(() => {
    setSections(buildSections({
      categories,
      orders,
      sources,
      mediaTypes,
      currentCategories,
      currentOrder,
      currentSources,
      currentMediaTypes
    }));
  }, [currentOrder, currentMediaTypes, currentSources, currentCategories, categories, orders, sources, mediaTypes]);

  useEffect(() => {
    window.addEventListener('resize', handleWindowWidthChange);
    handleWindowWidthChange({isPermanent, setPermanent, theme});
    return () => {
      window.removeEventListener('resize', handleWindowWidthChange);
    }
  }, [isPermanent, theme]);

  //handlers
  const setSidebarOpen = (e, externalForce = false) => {
    if ((externalForce || (wrapperRef.current && !wrapperRef.current.contains(e.target))) && !headerRef.current.contains(e.target) && !IS_WIDE_SCREEN) {
      dispatch(LayoutActions.setSidebarOpen(false));
    }
  };

  const handleWindowWidthChange = ({isPermanent, setPermanent, theme}) => () => {
    const windowWidth = window.innerWidth;
    const breakpointWidth = theme.breakpoints.values.md;
    const isSmallScreen = windowWidth < breakpointWidth;

    if (isSmallScreen && isPermanent) {
      setPermanent(false);
    } else if (!isSmallScreen && !isPermanent) {
      setPermanent(true);
    }
  };

  const handlePanelExpansion = (value) => {
    const result = currentExpanded.includes(value) ? currentExpanded.filter(e => e !== value) : currentExpanded.concat(value);

    setCurrentExpanded(result);
  };

  const handleSectionComponentItemClick = ({
                                             sectionName,
                                             dataItem,
                                             isChecked
                                           }) => {
    const mutateStateArray = (array, el) => !isChecked ? array.concat(el) : array.filter(e => e !== el);

    switch (sectionName) {
      case CATEGORY_SECTION_NAME:
        ContentActions.changeCategories(mutateStateArray(currentCategories, dataItem.label))(dispatch);
        break;
      case ORDERS_SECTION_NAME:
        ContentActions.changeOrder(dataItem.label)(dispatch);
        break;
      case SOURCES_SECTION_NAME:
        ContentActions.changeSources(mutateStateArray(currentSources, dataItem.label))(dispatch);
        break;
      case MEDIA_TYPE_SECTION_NAME:
        ContentActions.changeMediaTypes(mutateStateArray(currentMediaTypes, dataItem.label))(dispatch);
        break;
      default:
    }
  };

  return useMemo(() => <SidebarView
    wrapperRef={wrapperRef}
    isSidebarOpened={layoutState.isSidebarOpened}
    isPermanent={isPermanent}
    setSidebarOpen={setSidebarOpen}
    currentCategories={currentCategories}
    currentOrder={currentOrder}
    currentSources={currentSources}
    currentMediaTypes={currentMediaTypes}
    sections={sections}
    currentExpanded={currentExpanded}
    handlePanelExpansion={handlePanelExpansion}
    handleSectionComponentItemClick={handleSectionComponentItemClick}
  />, [
    wrapperRef,
    layoutState.isSidebarOpened,
    isPermanent,
    currentCategories,
    currentOrder,
    currentSources,
    currentMediaTypes,
    sections,
    currentExpanded
  ]);
};

export default withRouter(SideBarController);
