import React from 'react';
import withStyles from "@material-ui/core/styles/withStyles";
import {Button, Dialog, DialogActions, DialogContent, DialogTitle} from "@material-ui/core";

import styles from './styles';
import CustomLoadingBar from "../LoadingBar/custom-loading-bar";
import {IS_WIDE_SCREEN} from "../../state";

const FormDialog = ({
                      classes,
                      isOpen,
                      handleClose,
                      actionHandler,
                      actionTitle,
                      disabled,
                      title,
                      content,
                      isLoading
                    }) => {
  return (<Dialog open={isOpen}
                  onClose={handleClose}
                  disableBackdropClick={!IS_WIDE_SCREEN}
                  aria-labelledby="form-dialog-title"
                  classes={{
                    root: classes.dialogRoot,
                    paper: classes.dialogPaper
                  }}>
    <DialogTitle id="form-dialog-title"
                 classes={{
                   root: classes.dialogTitleRoot
                 }}
    >{title}
    </DialogTitle>
    <DialogContent classes={{
      root: classes.dialogContentRoot
    }}>
      {content}
    </DialogContent>
    {
      <DialogActions>
        <Button onClick={handleClose} onTouchEnd={handleClose} color="primary">
          Cancel
        </Button>
        {actionTitle && <Button onClick={actionHandler} onTouchEnd={actionHandler} disabled={disabled} color="primary">
          {actionTitle}
        </Button>}
      </DialogActions>
    }
    {isLoading ? <CustomLoadingBar
      classes={{
        root: classes.loadingBar
      }}
    /> : <div/>}
  </Dialog>);
};

export default withStyles(styles, {withTheme: true})(FormDialog);
