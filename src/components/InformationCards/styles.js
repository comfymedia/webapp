export default theme => ({
  card: {
    display: "inline-flex",
    flexDirection: "column",
    width: "99%",
    marginTop: theme.spacing(),
    background: theme.palette.background.shaded,
    height: 50
  },
  cardContentRoot: {
    padding: 0
  },
  content: {
    display: "flex",
    alignItems: "center",
    justifyContent: "left"
  },
  bar: {
    height: 50,
    width: 5
  },
  message: {
    marginBottom: 0,
    marginLeft: 10,
    display: 'flex',
    alignItems: 'center',
  },
  messageText: {
    marginLeft: 10
  }
});
