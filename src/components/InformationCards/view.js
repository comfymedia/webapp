import React from 'react';
import styles from './styles';
import {Card, CardContent, withStyles} from "@material-ui/core";
import {Info} from "@material-ui/icons";

const View = ({
                classes,
                theme,
                color = 'info',
                message,
                icon = Info
              }) => {
  let mappedColor;
  switch (color) {
    case 'warning':
      mappedColor = theme.palette.warning.main;
      break;
    case 'error':
      mappedColor = theme.palette.error.main;
      break;
    case 'success':
      mappedColor = theme.palette.success.main;
      break;
    default:
      mappedColor = theme.palette.info.main;
  }

  return (
    <Card
      elevation={0}
      className={classes.card}>
      <CardContent className={classes.content} classes={{
        root: classes.cardContentRoot
      }}>
        <span className={classes.bar} style={{
          background: mappedColor
        }}/>
        <span className={classes.message}>
          {React.cloneElement(icon, {
            style: {
              color: mappedColor
            }
          })}
          <span className={classes.messageText}>
            {message}
          </span>
        </span>
      </CardContent>
    </Card>
  )
};

export default withStyles(styles, {withTheme: true})(View);
