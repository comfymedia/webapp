import React from 'react';
import {CircularProgress, withStyles} from "@material-ui/core";
import styles from "./styles";

const CustomLoadingBar = ({
                            classes
                          }) => {
  return (
    <div className={classes.root}>
      <CircularProgress className={classes.progress} variant="indeterminate" />
    </div>
  );
};

export default withStyles(styles, {withTheme: true})(CustomLoadingBar);
