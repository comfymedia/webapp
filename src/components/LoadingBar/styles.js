export default theme => ({
  root: {
    position: 'absolute',
    top: '50%',
    display: 'flex',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'space-around'
  }
})
