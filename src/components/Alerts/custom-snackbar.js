import React from 'react';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import ErrorIcon from '@material-ui/icons/Error';
import InfoIcon from '@material-ui/icons/Info';
import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import WarningIcon from '@material-ui/icons/Warning';
import {withStyles} from '@material-ui/core/styles';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import styles from './styles';
import {Snackbar} from "@material-ui/core";
import {makeStyles} from "@material-ui/styles";

const variantIcon = {
  success: CheckCircleIcon,
  warning: WarningIcon,
  error: ErrorIcon,
  info: InfoIcon,
};

const CustomSnackBar = ({
                          classes,
                          overrides,
                          anchorOrigin,
                          className,
                          message,
                          onClose,
                          variant,
                          open,
                          ...other
                        }) => {
  const Icon = variantIcon[variant];

  const useStyles = makeStyles({
    ...overrides
  });

  return (
    <Snackbar
      classes={{
        ...useStyles()
      }}
      anchorOrigin={anchorOrigin}
      className={className}
      open={open}
      onClose={onClose}
    >
      <SnackbarContent
        className={clsx(classes[variant], className)}
        aria-describedby="client-snackbar"
        message={
          <span id="client-snackbar" className={classes.message}>
          <Icon className={clsx(classes.icon, classes.iconVariant)}/>
            {message}
        </span>
        }
        classes={{
          message: classes.snackBarContentMessageContainer
        }}
        action={[
          <IconButton key="close" aria-label="Close" color="inherit" onClick={onClose}>
            <CloseIcon className={classes.icon}/>
          </IconButton>,
        ]}
        {...other}
      />
    </Snackbar>
  );
};

CustomSnackBar.propTypes = {
  classes: PropTypes.object,
  className: PropTypes.string,
  message: PropTypes.node,
  open: PropTypes.bool,
  onClose: PropTypes.func,
  variant: PropTypes.oneOf(['success', 'warning', 'error', 'info']).isRequired,
};

export default withStyles(styles, {withTheme: true})(CustomSnackBar);
