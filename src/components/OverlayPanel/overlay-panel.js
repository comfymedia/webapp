import React, {useMemo} from 'react';
import withStyles from "@material-ui/core/styles/withStyles";
import {Divider, Drawer, IconButton} from "@material-ui/core";

import {Close as CloseIcon} from "@material-ui/icons";
import {IS_WIDE_SCREEN} from "../../state";

import styles from './styles';
import CustomLoadingBar from "../LoadingBar/custom-loading-bar";

const OverlayPanel = ({
                        classes,
                        theme,
                        content,
                        isOpen,
                        handleClose,
                        title,
                        isLoading
                      }) => {
  const _handleClose = (event, reason) => {
    handleClose();
  };

  return useMemo(() =>
    <Drawer
      anchor="right"
      open={isOpen}
      disableBackdropClick={!IS_WIDE_SCREEN}
      onClose={_handleClose}
    >
      <div className={classes.panelWrapper} role="presentation">
        <div className={classes.panelHeader}>
          {!IS_WIDE_SCREEN ? <IconButton
            color="inherit"
            onClick={_handleClose}
            className={classes.headerCloseAction}
            TouchRippleProps={{
              classes: {
                child: classes.headerActionRippleChild,
                rippleVisible: classes.headerActionRippleVisible
              }
            }}
            onTouchEnd={_handleClose}
          >
            <CloseIcon/>
          </IconButton> : null}
          <span className={classes.headerTitle}>
            {title}
          </span>
        </div>
        <Divider/>
        <div className={classes.panelContent}>
          {content}
        </div>
        {isLoading ? <CustomLoadingBar
          classes={{
            root: classes.loadingBar
          }}
        /> : <div/>}
      </div>
    </Drawer>, [classes, content, isOpen, title, _handleClose]);
};

export default withStyles(styles, {withTheme: true})(OverlayPanel);
