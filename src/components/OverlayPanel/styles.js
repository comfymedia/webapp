export default theme => ({
  panelWrapper: {
    position: 'relative',
    [theme.breakpoints.between("xs", "md")]: {
      width: '100vw'
    },
    [theme.breakpoints.up("md")]: {
      width: 500
    }
  },
  panelHeader: {
    [theme.breakpoints.between("xs", "md")]: {
      display: 'flex',
      alignItems: 'center'
    },
    padding: theme.spacing()
  },
  panelContent: {},
  headerTitle: {
    [theme.breakpoints.up("md")]: {
      margin: `${theme.spacing()}px ${theme.spacing()}px ${theme.spacing()}px ${theme.spacing()}px`,
    },
    [theme.breakpoints.between("xs", "md")]: {
      margin: theme.spacing(),
    },
    color: theme.palette.text.primary,
    fontSize: 15,
    fontWeight: 'bold'
  },
  headerCloseAction: {
    backgroundColor: theme.palette.primary.light,
    padding: theme.spacing(),
    margin: theme.spacing(),
    ['& span']: {
      height: theme.spacing(),
      width: theme.spacing()
    },
    ['& svg']: {
      height: theme.spacing(2)
    }
  },
  headerActionRippleChild: {
    backgroundColor: theme.palette.primary.light
  },
  headerActionRippleVisible: {
    opacity: 0.5
  }
});
