export const ALERTS_RESET = "Alerts/RESET";
export const ALERTS_ADD = "Alerts/ADD";

const resetAlert = () => dispatch => (dispatch({
  type: ALERTS_RESET
}));

const addErrorAlert = (message = 'An error has occurred.') => dispatch => (dispatch({
  type: ALERTS_ADD,
  payload: {
    variant: "error",
    message
  }
}));

const addSuccessAlert = (message = 'Great success.') => dispatch => (dispatch({
  type: ALERTS_ADD,
  payload: {
    variant: "success",
    message
  }
}));

export default {
  resetAlert,
  addErrorAlert,
  addSuccessAlert
}
