import {ALERTS_ADD, ALERTS_RESET} from './actions';

export const notificationInitialState = {
  open: false,
  variant: "success",
  message: null
};

export default (state = notificationInitialState, {type, payload}) => {
  switch (type) {
    case ALERTS_RESET:
      return {
        ...state,
        open: false,
        message: null
      };
    case ALERTS_ADD:
      return {
        ...state,
        open: true,
        message: payload.message,
        variant: payload.variant
      };
    default:
      return state;
  }
};
