import {
  CONTENT_CHANGE_CATEGORIES,
  CONTENT_CHANGE_MEDIA_TYPES,
  CONTENT_CHANGE_ORDER,
  CONTENT_CHANGE_SOURCES,
  CONTENT_MULTIPLY
} from './actions';
import {DEFAULT_CATEGORIES, DEFAULT_ORDERS, DEFAULT_SOURCES} from "../../clients/comfy";
import {DEFAULT_MEDIA_TYPES} from "../../clients/comfy/models";

export const contentInitialState = {
  categories: [DEFAULT_CATEGORIES.RANDOM],
  order: DEFAULT_ORDERS.TRENDING,
  sources: Object.keys(DEFAULT_SOURCES),
  mediaTypes: Object.keys(DEFAULT_MEDIA_TYPES),
  multiply: false
};

export default (state = contentInitialState, {type, payload}) => {
  switch (type) {
    case CONTENT_CHANGE_CATEGORIES:
      return {
        ...state,
        categories: payload.categories
      };
    case CONTENT_CHANGE_ORDER:
      return {
        ...state,
        order: payload.order
      };
    case CONTENT_CHANGE_SOURCES:
      return {
        ...state,
        sources: payload.sources
      };
    case CONTENT_CHANGE_MEDIA_TYPES:
      return {
        ...state,
        mediaTypes: payload.mediaTypes
      };
    case CONTENT_MULTIPLY:
      return {
        ...state,
        multiply: payload.multiply
      };
    default:
      return state;
  }
};
