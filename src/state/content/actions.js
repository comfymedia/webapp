export const CONTENT_CHANGE_CATEGORIES = "Content/CONTENT_CHANGE_CATEGORIES";
export const CONTENT_CHANGE_ORDER = "Content/CONTENT_CHANGE_ORDER";
export const CONTENT_CHANGE_SOURCES = "Content/CONTENT_CHANGE_SOURCES";
export const CONTENT_CHANGE_MEDIA_TYPES = "Content/CONTENT_CHANGE_MEDIA_TYPES";
export const CONTENT_MULTIPLY = "Content/CONTENT_MULTIPLY";

const changeCategories = (categories) => dispatch => (dispatch({
  type: CONTENT_CHANGE_CATEGORIES,
  payload: {
    categories
  }
}));

const changeSources = (sources) => dispatch => (dispatch({
  type: CONTENT_CHANGE_SOURCES,
  payload: {
    sources
  }
}));

const changeOrder = (order) => dispatch => (dispatch({
  type: CONTENT_CHANGE_ORDER,
  payload: {
    order
  }
}));

const changeMediaTypes = (mediaTypes) => dispatch => (dispatch({
  type: CONTENT_CHANGE_MEDIA_TYPES,
  payload: {
    mediaTypes
  }
}));

const changeMultiply = (multiply) => dispatch => (dispatch({
  type: CONTENT_MULTIPLY,
  payload: {
    multiply
  }
}));

export default {
  changeCategories,
  changeSources,
  changeOrder,
  changeMediaTypes,
  changeMultiply
}
