import {SET_SIDEBAR_STATUS, TOGGLE_SIDEBAR} from "./actions";

const VIEW_PORT_WIDTH = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);

export const IS_WIDE_SCREEN = VIEW_PORT_WIDTH > 900;

export const initialState = {
  isSidebarOpened: IS_WIDE_SCREEN
};

export default function LayoutReducer(state = initialState, { type, payload }) {
  switch (type) {
    case TOGGLE_SIDEBAR:
      return {
        ...state,
        isSidebarOpened: !state.isSidebarOpened,
      };
    case SET_SIDEBAR_STATUS:
      return {
        ...state,
        isSidebarOpened: payload,
      };
    default:
      return state;
  }
}
