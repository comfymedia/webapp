export const TOGGLE_SIDEBAR = "Layout/TOGGLE_SIDEBAR";
export const SET_SIDEBAR_STATUS = "Layout/SET_SIDEBAR_STATUS";

export const toggleSidebar = () => ({
  type: TOGGLE_SIDEBAR
});

export const setSidebarOpen = (payload) => ({
  type: SET_SIDEBAR_STATUS,
  payload: payload
});

export default {
  toggleSidebar,
  setSidebarOpen
};
