# base image
FROM nginx:1.16.0-alpine

RUN mkdir /usr/app

# set working directory
WORKDIR /usr/app

ADD provision/run.sh run.sh

#copy app content
ADD build /usr/share/nginx/html

#remove nginx default config
RUN rm /etc/nginx/conf.d/default.conf

#copy custom nginx config
COPY provision/nginx.conf /etc/nginx/conf.d

#expose nginx http port
EXPOSE 8080

# start app
ENTRYPOINT ["sh", "./run.sh"]